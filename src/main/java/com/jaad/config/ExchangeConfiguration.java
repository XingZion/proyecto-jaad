package com.jaad.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ExchangeConfiguration {

    @Bean(name = "apiConnection")
    public RestTemplate apiConnection(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }
}
