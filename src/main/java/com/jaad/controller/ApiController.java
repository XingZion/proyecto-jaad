package com.jaad.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import com.jaad.entities.Commit;
import com.jaad.entities.Values;
import com.jaad.service.ExchangeService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jaad.dto.CommitAuthor;

@RestController()
@RequestMapping("/v1/")
public class ApiController {
    @Autowired
    private ExchangeService exchangeService;

    @GetMapping(value = "/index", produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public Map<String, Object> index() throws IOException {
        StringBuffer content = new StringBuffer();
        Map<String, Object> response = new HashMap<String, Object>();
        try {
            URL url = new URL("https://api.bitbucket.org/2.0/repositories/XingZion/proyecto-jaad/commits");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application.properties/json");

            int status = con.getResponseCode();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();

            List<CommitAuthor> listaCommit = new ArrayList<>();

            JSONObject lastestCommit = new JSONObject(content.toString());
            JSONArray arrayCommit = lastestCommit.getJSONArray("values");
            arrayCommit.forEach((x) -> {
                System.out.println("OBJECT => " + x);

                JSONObject objCommit = new JSONObject(x.toString());
                CommitAuthor commit = new CommitAuthor();

                commit.setAutor(objCommit.get("author").toString());
                commit.setFecha(objCommit.get("date").toString());
                commit.setProyecto(objCommit.get("hash").toString());
                commit.setMensaje(objCommit.get("type").toString());
                listaCommit.add(commit);
            });
            response.put("code", "200");
            response.put("data", listaCommit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /* URL::: http://localhost:8080/v1/test*/
    @GetMapping(value = "/{proyecto}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CommitAuthor> getCommit(@PathVariable String proyecto) {
        Commit commit = exchangeService.getCommit(proyecto);
        List<CommitAuthor> commitAuthorList = new ArrayList<>();
        for (Values values : commit.getValues()) {
            CommitAuthor commitAuthor = new CommitAuthor();
            Optional.ofNullable(values.getAuthor().getUser()).ifPresent(user -> {
                        Optional.ofNullable(user.getDisplay_name()).ifPresent(displayName -> commitAuthor.setAutor(displayName));
                    }
            );
            Optional.ofNullable(values.getDate()).ifPresent(fecha -> {
                commitAuthor.setFecha(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(fecha));
            });
            Optional.ofNullable(values.getRepository().getName()).ifPresent(name -> commitAuthor.setProyecto(name));
            Optional.ofNullable(values.getMessage()).ifPresent(mensaje -> commitAuthor.setMensaje(mensaje));
            commitAuthorList.add(commitAuthor);
        }
        return commitAuthorList;
    }

    /* URL::: http://localhost:8080/v1/{proyecto}/{nombre}*/
    @GetMapping(value = "/{proyecto}/{nombre}")
    @ResponseBody
    public ResponseEntity<Object> getCommitByAuthor(@PathVariable String proyecto, @PathVariable String nombre) {

        List<CommitAuthor> commitsAuhor = new ArrayList<>();
        StringBuffer content = new StringBuffer();

        try {

            URL url = new URL("https://api.bitbucket.org/2.0/repositories/XingZion/" + proyecto + "/commits");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application.properties/json");

            int status = con.getResponseCode();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            JSONObject lastestCommit = new JSONObject(content.toString());

            JSONArray valuesRepositorio = lastestCommit.getJSONArray("values");

            for (int i = 0; i < valuesRepositorio.length(); i++) {

                try {

                    JSONObject datosJsonCommit = valuesRepositorio.getJSONObject(i);
                    ///System.out.println("objeto " + i + " -> " +datosJsonCommit);

                    String autor = datosJsonCommit.getJSONObject("author").getJSONObject("user").getString("display_name");
                    //System.out.println("autor ------>" + autor);
                    if (autor != null) {

                        if (autor.contains(nombre)) {

                            String fechaCommit = datosJsonCommit.getString("date");
                            String nameProyecto = datosJsonCommit.getJSONObject("repository").getString("name");

                            CommitAuthor commitAuthor = new CommitAuthor();
                            commitAuthor.setAutor(autor);
                            commitAuthor.setFecha(fechaCommit);
                            commitAuthor.setProyecto(nameProyecto);
                            commitAuthor.setMensaje("");
                            commitsAuhor.add(commitAuthor);

                        }
                    }

                } catch (Exception e) {

                }
            }

        } catch (Exception e) {
            e.printStackTrace();

        }

        return ResponseEntity.ok(commitsAuhor);

    }

}
