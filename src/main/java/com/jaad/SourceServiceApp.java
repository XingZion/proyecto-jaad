package com.jaad;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SourceServiceApp {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SourceServiceApp.class);
        application.setBannerMode(Banner.Mode.OFF);
        application.run(args);
    }
}
