package com.jaad.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Commit implements Serializable {
    private String pagelen;
    private Values[] values;
    private Author author;

    public String getPagelen() {
        return pagelen;
    }

    public void setPagelen(String pagelen) {
        this.pagelen = pagelen;
    }

    public Values[] getValues() {
        return values;
    }

    public void setValues(Values[] values) {
        this.values = values;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Commit{" +
                "pagelen='" + pagelen + '\'' +
                ", values=" + Arrays.toString(values) +
                ", author=" + author +
                '}';
    }
}
