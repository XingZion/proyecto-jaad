package com.jaad.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Values implements Serializable {
    private String hash;
    private Date date;
    private String message;
    private String type;
    private Rendered rendered;
    private Author author;
    private Summary summary;
    private Repository repository;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Rendered getRendered() {
        return rendered;
    }

    public void setRendered(Rendered rendered) {
        this.rendered = rendered;
    }

    @Override
    public String toString() {
        return "Values{" +
                "hash='" + hash + '\'' +
                ", date=" + date +
                ", message='" + message + '\'' +
                ", type='" + type + '\'' +
                ", rendered=" + rendered +
                ", author=" + author +
                ", summary=" + summary +
                ", repository=" + repository +
                '}';
    }
}
