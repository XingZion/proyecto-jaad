package com.jaad.service;

import com.jaad.entities.Commit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ExchangeService {
    private final RestTemplate apiConnection;
    private final static Logger LOGGER = LoggerFactory.getLogger(ExchangeService.class);

    public ExchangeService(@Qualifier("apiConnection") final RestTemplate apiConnection) {
        this.apiConnection = apiConnection;
    }

    public Commit getCommit(String proyecto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Commit> entity = new HttpEntity<>(headers);
        ResponseEntity<Commit> responseEntity = apiConnection.exchange("https://api.bitbucket.org/2.0/repositories/XingZion/"+proyecto+"/commits",
                HttpMethod.GET, entity, Commit.class);
        return responseEntity.getBody();
    }
}
